package com.example.todo_app;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity
{
    //initialise data here
    private static final String TODO_INDEX = "com.example.todoIndex";
    private int todoIndex;
    private static final String STATUS = "com.example.status";
    private boolean[] status;

    //init any classes
    private DisplayManager displayManager;

    public static Intent newIntent(Context packageContext, int index, boolean[] statusArray)
    {
        Intent intent = new Intent(packageContext, DetailsActivity.class);
        // set intent values
        intent.putExtra(TODO_INDEX, index);
        intent.putExtra(STATUS, statusArray);
        return intent;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) // move to state manager
    {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(TODO_INDEX, todoIndex);
        savedInstanceState.putBooleanArray(STATUS, status);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState); //call super class to complete activity creation
        setContentView(R.layout.activity_details); //set UI layout

        //get intent vals
        todoIndex = getIntent().getIntExtra(TODO_INDEX, 0);
        status = getIntent().getBooleanArrayExtra(STATUS);

        displayManager = new DisplayManager();

        //load state
        loadState(savedInstanceState);

        //display first task
        displayManager.updateView();

        //add button listeners
        configButtons();
    }

    private void configButtons()
    {
        ButtonListener listener = new ButtonListener();
        findViewById(R.id.checkBoxComplete).setOnClickListener(listener);
    }

    private class ButtonListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v)
        {


            switch (v.getId())
            {
                case R.id.checkBoxComplete:
                    CheckBox box = findViewById(R.id.checkBoxComplete);
                    setStatus(box.isChecked());
                    finish();
                    break;
                //case R.id.checkBoxDelete:
                //    break;
                default:
                    break;
            }
        }
    }

    private class DisplayManager
    {
        private TextView detailTextView;
        private CheckBox completeCheckBox;
        private String[] detailStrings;

        private Resources res;

        private DisplayManager()
        {
            res = getResources();

            detailTextView = findViewById(R.id.textViewDetail);
            completeCheckBox = findViewById(R.id.checkBoxComplete);
            detailStrings = res.getStringArray(R.array.descriptions);
        }

        private void updateView()
        {
            detailTextView.setText(detailStrings[todoIndex]);
            completeCheckBox.setChecked(status[todoIndex]);
        }
    }

    private void loadState(Bundle savedInstanceState)
    {
        //restore state
        if(savedInstanceState != null)
        {
            todoIndex = savedInstanceState.getInt(TODO_INDEX);
            status = savedInstanceState.getBooleanArray(STATUS);
        }
    }

    private void setStatus(boolean isChecked)
    {
        status[todoIndex] = isChecked;

        Intent intent = new Intent();
        intent.putExtra(STATUS, status);
        setResult(RESULT_OK, intent);
    }
}
