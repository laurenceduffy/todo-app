package com.example.todo_app;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class TodoActivity extends AppCompatActivity
{
    //initialise data here
    private static final String TODO_INDEX = "com.example.todoIndex";
    private int todoIndex = 0;
    private static final String STATUS = "com.example.status";
    private boolean[] status;

    private int IS_SUCCESS; //return value from intent

    //helpers
    private DisplayManager displayManager;

    public static final String TAG = "TodoActivity";

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) // move to state manager
    {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(TODO_INDEX, todoIndex);
        savedInstanceState.putBooleanArray(STATUS, status);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState); //call super class to complete activity creation
        setContentView(R.layout.activity_todo); //set UI layout

        //init any data
        status = getStatusArray();

        //init any classes
        displayManager = new DisplayManager();

        //load state
        loadState(savedInstanceState);

        //display first task
        displayManager.updateView(this);

        //add button listeners
        configButtons();
    }
    
    public boolean[] getStatusArray() // convert int array to bool array
    {
        int[] temp = getResources().getIntArray(R.array.status);

        boolean[] retval = new boolean[temp.length];
        int i = 0;

        for (int val : temp)
        {
            retval[i] = (val == 1);
            i++;
        }

        return retval;
    }

    private void configButtons()
    {
        ButtonListener listener = new ButtonListener();
        findViewById(R.id.buttonNext).setOnClickListener(listener);
        findViewById(R.id.buttonPrev).setOnClickListener(listener);
        findViewById(R.id.buttonDetail).setOnClickListener(listener);
    }

    private class ButtonListener implements View.OnClickListener
    {
        private ButtonListener()
        {
            super();
        }

        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.buttonNext:
                    incrementTodoIndex(1);
                    break;
                case R.id.buttonPrev:
                    incrementTodoIndex(-1);
                    break;
                case R.id.buttonDetail:
                    Intent intent = DetailsActivity.newIntent(TodoActivity.this, todoIndex, status);
                    startActivityForResult(intent, IS_SUCCESS);
                    break;
                default:
                    break;
            }
        }
    }

    // Manage the looping or limiting of an index for the list between max and min values
    private void incrementTodoIndex(int amount)
    {
        int maxIndex = displayManager.getTodoStrings().length - 1;
        int minIndex = 0;
        int newIndex = todoIndex + amount;

        if (newIndex > maxIndex)
        {
            Log.d(TAG, "[!] todo index too high - reset to " + minIndex);
            todoIndex = minIndex;
        }
        else if (newIndex < minIndex)
        {
            Log.d(TAG, "[!] todo index too low - reset to " + maxIndex);
            todoIndex = maxIndex;
        }
        else
        {
            todoIndex = newIndex;
        }

        displayManager.updateView(this);
    }

    //handle text changes etc.
    private class DisplayManager
    {
        private TextView todoTextView;
        private TextView completeTextView;
        private String[] todoStrings;

        private Resources res;

        private DisplayManager()
        {
            res = getResources();

            todoTextView = findViewById(R.id.textViewItem);
            completeTextView = findViewById(R.id.textViewComplete);
            todoStrings = res.getStringArray(R.array.todos);
        }

        private void updateView(Context context)
        {
            todoTextView.setText(todoStrings[todoIndex]);
            if(status[todoIndex])
            {
                completeTextView.setText("Complete"); // TODO: update color
                completeTextView.setTextColor(ContextCompat.getColor(context, R.color.colorSuccess));
            }
            else
            {
                completeTextView.setText("On-going Task"); // TODO: update color
                completeTextView.setTextColor(ContextCompat.getColor(context, R.color.colorFailure));
            }
        }

        private String[] getTodoStrings()
        {
            return todoStrings;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        if (requestCode == IS_SUCCESS )
        {
            if (intent != null)
            {
                // data in intent from child activity
                status = intent.getBooleanArrayExtra(STATUS);
                displayManager.updateView(this);
            }
            else
            {
                //back button pressed
            }
        }
        else
        {
            // intent request failure
        }
    }

    private void loadState(Bundle savedInstanceState)
    {
        //restore state
        if(savedInstanceState != null)
        {
            todoIndex = savedInstanceState.getInt(TODO_INDEX);
            status = savedInstanceState.getBooleanArray(STATUS);
        }
    }
}
